//
//  ModuleBuilder.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import UIKit
import CoreData

class ModuleBuilder: ModuleBuilderProtocol {
    static func createMainModule() -> UIViewController {
        let view = MainView()
        let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext
        let networkService = NetwortService()
        let viewModel = MainViewModel(networkService: networkService)
        let viewController = MainViewController(mainView: view, viewModel: viewModel)
        viewController.context = context
        
        return viewController
    }
}

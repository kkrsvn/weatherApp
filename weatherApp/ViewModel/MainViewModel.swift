//
//  MainViewModel.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import CoreLocation
import UIKit

class MainViewModel: MainViewModelProtocol {
    
    var networkService: NetworkServiceProtocol
    
    required init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func updateCityWeather(city: String) {
        self.networkService.fetchCurrentWeather(forRequestType: .cityName(city: city))
    }
    
    func updateLocationWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.networkService.fetchCurrentWeather(forRequestType: .coordinate(latitude: latitude, longitude: longitude))
    }
}

//
//  Protocols.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 17.09.2022.
//

import UIKit
import CoreLocation


protocol ModuleBuilderProtocol: AnyObject {
    static func createMainModule() -> UIViewController
}

protocol MainViewProtocol: UIView {
    var weatherData: WeatherModel? { get set }
    func setCurrentWeather(weatherModel: WeatherModel)
    func loadingWeather()
    func setWeather()
}

protocol MainViewModelProtocol: AnyObject {
    var networkService: NetworkServiceProtocol { get set }
    func updateCityWeather(city: String)
    func updateLocationWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
}

protocol NetworkServiceProtocol: AnyObject {
    var onComplition: ((WeatherModel) -> Void)? { get set }
    func fetchCurrentWeather(forRequestType requestType: RequestType)
}

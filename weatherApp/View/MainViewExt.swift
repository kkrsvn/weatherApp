//
//  MainViewExt.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import UIKit

extension MainView {
    
    // MARK: - Значек погоды
    func makeWeatherIconImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.tintColor = .white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(imageView)
        
        imageView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
        imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        return imageView
    }
    
    // MARK: -  Лэйбл Основной Температуры
    func makeTemperatureLable() -> UILabel {
        let lable = UILabel()
        lable.font = .systemFont(ofSize: 60, weight: .semibold)
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(lable)
        
        lable.topAnchor.constraint(equalTo: self.weatherIconImageView.bottomAnchor, constant: 16).isActive = true
        lable.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        return lable
    }
    
    // MARK: - Лэйбл Дополнительной Температуры
    func makeFeelsLikeTempratureLable() -> UILabel {
        let lable = UILabel()
        lable.font = .systemFont(ofSize: 16, weight: .regular)
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .right
        lable.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(lable)
        
        lable.topAnchor.constraint(equalTo: self.temperatureLable.bottomAnchor, constant: 0).isActive = true
        lable.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        return lable
    }
    
    // MARK: - Лэйбл Города
    func makeCityLable() -> UILabel {
        let lable = UILabel()
        lable.font = .systemFont(ofSize: 20, weight: .regular)
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .right
        lable.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(lable)
        
        lable.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        lable.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -75).isActive = true
        
        return lable
    }
}

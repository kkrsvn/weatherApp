//
//  MainView.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 18.09.2022.
//

import UIKit

class MainView: UIView, MainViewProtocol {

    var weatherData: WeatherModel? {
        didSet {
            DispatchQueue.main.async {
                self.setNeedsLayout()
            }
        }
    }
    
    lazy var weatherIconImageView = makeWeatherIconImageView()
    lazy var temperatureLable = makeTemperatureLable()
    lazy var feelsLikeTempratureLable = makeFeelsLikeTempratureLable()
    lazy var cityLable = makeCityLable()
    lazy var activityIndicator = makeActivityIndicator()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let data = self.weatherData else { return }
        self.setCurrentWeather(weatherModel: data)
    }
    
    // MARK: - Установка погоды
    func setCurrentWeather(weatherModel: WeatherModel) {
        self.temperatureLable.text = weatherModel.temperatureString
        self.feelsLikeTempratureLable.text = weatherModel.feelLikeTempratureString
        self.weatherIconImageView.image = UIImage(systemName: weatherModel.systemIconStirng, withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 100)))
        self.cityLable.text = weatherModel.cityName
        
        self.setWeather()
    }
}

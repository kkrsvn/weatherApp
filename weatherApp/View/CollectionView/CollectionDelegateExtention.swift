//
//  CollectionDelegateExtention.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit
import CoreData

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isDeleteEnable {
            let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
            if let cities = try? self.context.fetch(fetchRequest) {
                context.delete(cities[indexPath.row])
            }
            do {
                try context.save()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            self.cities.remove(at: indexPath.row)
            collectionView.deselectItem(at: indexPath, animated: false)
            self.collectionView.reloadData()
        } else {
            let currentCell = collectionView.cellForItem(at: indexPath) as! CustomCell
            let city = currentCell.cityName.split(separator: " ").joined(separator: "%20")
            self.viewModel.updateCityWeather(city: city)
            self.mainView.loadingWeather()
        }
    }
}

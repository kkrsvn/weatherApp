//
//  CollectionViewExtention.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit

extension MainViewController {
    
    func makeLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 1.0
        return layout
    }
    
    func makeCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(CustomCell.self, forCellWithReuseIdentifier: "CustomCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(collection)

        collection.bottomAnchor.constraint(equalTo: self.findButtonView.topAnchor, constant: -8).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -65).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 65).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        return collection
    }
}

//
//  CustomCell.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit

class CustomCell: UICollectionViewCell {
    
    var cityName: String = ""
    
    private lazy var image: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 16)))
        imageView.tintColor = .white
        return imageView
    }()
    
    private lazy var imageConstraints = [
        self.image.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
        self.image.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.image)
        self.setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.imageConstraints)
    }
}

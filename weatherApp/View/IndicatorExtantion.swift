//
//  IndicatorExtantion.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit

extension MainView {
    
    // MARK: - Индикатор загрузки
    func makeActivityIndicator() -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.color = .white
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(indicator)
        
        indicator.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
        indicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
       
        return indicator
    }
    
    // MARK: - Показать индикатор
    func loadingWeather() {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.weatherIconImageView.isHidden = true
        self.cityLable.isHidden = true
        self.temperatureLable.isHidden = true
        self.feelsLikeTempratureLable.isHidden = true
    }
    
    // MARK: - Испрятать индикатор
    func setWeather() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        
        self.weatherIconImageView.isHidden = false
        self.cityLable.isHidden = false
        self.temperatureLable.isHidden = false
        self.feelsLikeTempratureLable.isHidden = false
    }
}

//
//  AlertViewExtention.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import UIKit

extension MainViewController {
    func presentSearchAlertController(withTitle title: String?, message: String?, style: UIAlertController.Style, completionHandler: @escaping (String) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addTextField { textField in
            textField.placeholder = "Введите город"
            textField.borderStyle = .roundedRect
        }
        let search = UIAlertAction(title: "Искать", style: .default) { action in
            let textField = alert.textFields?.first
            guard let cityName = textField?.text else { return }
            if cityName != "" {
                let city = cityName.split(separator: " ").joined(separator: "%20")
                completionHandler(city)
            }
        }
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addAction(search)
        alert.addAction(cancel)
        alert.view.addSubview(UIView())
        present(alert, animated: false, completion: nil)
    }
}

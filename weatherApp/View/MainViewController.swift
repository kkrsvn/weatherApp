//
//  ViewController.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import UIKit
import CoreLocation
import CoreData

class MainViewController: UIViewController {
    
    var cities: [City] = []
    var isDeleteEnable: Bool = false
    
    var mainView: MainViewProtocol!
    var viewModel: MainViewModelProtocol!
    var context: NSManagedObjectContext!
    var locationManager: CLLocationManager!
    var activityIndicator: UIActivityIndicatorView!
    var findButtonView: UIImageView!
    var saveButtonView: UIImageView!
    var locationButtonView: UIImageView!
    var gestureFind: UITapGestureRecognizer!
    var gestureSave: UITapGestureRecognizer!
    var gestureCL: UITapGestureRecognizer!
    var gestureDelete: UILongPressGestureRecognizer!
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var backgroundView: UIImageView!

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required init(mainView: MainViewProtocol, viewModel: MainViewModelProtocol) {
        self.mainView = mainView
        self.viewModel = viewModel
        
        super .init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureFind = makeGestureFind()
        self.gestureSave = makeGestureSave()
        self.gestureCL = makeGestureCL()
        self.gestureDelete = makeGestureDelete()
        self.findButtonView = makeFindButtonView()
        self.saveButtonView = makeSaveButtonView()
        self.locationButtonView = makeСLButtonView()
        self.locationManager = makeLocationManager()
        self.layout = makeLayout()
        self.collectionView = makeCollectionView()
        self.backgroundView = makeBackGround()
        
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.requestLocation()
        }
        
        self.createView()
        self.updateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
        
        do {
            cities = try self.context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.mainView.loadingWeather()
    }
    
    private func updateView() {
        self.viewModel.networkService.onComplition = { [weak self] viewData in
            self?.mainView.weatherData = viewData
        }
    }
}

//
//  MainViewControllerExt.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 17.09.2022.
//

import UIKit

extension MainViewController {
    
    // MARK: - Фоновая картинка
    func makeBackGround() -> UIImageView {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "background")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.insertSubview(imageView, at: 0)
        
        imageView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        return imageView
    }
    
    // MARK: - Настройки основного view
    func createView() {
        self.mainView.translatesAutoresizingMaskIntoConstraints = false
        self.view.insertSubview(self.mainView, at: 1)
        self.mainView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.mainView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.mainView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.mainView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
}

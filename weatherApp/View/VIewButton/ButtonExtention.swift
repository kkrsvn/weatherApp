//
//  ButtonExtantion.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit

extension MainViewController {
    
    // MARK: - Кнопка поиска по названию города
    func makeFindButtonView() -> UIImageView {
        let imageView = UIImageView()
        let largeFont = UIFont.systemFont(ofSize: 25)
        let configuration = UIImage.SymbolConfiguration(font: largeFont)
        imageView.image = UIImage(systemName: "magnifyingglass.circle.fill", withConfiguration: configuration)
        imageView.tintColor = .white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(self.gestureFind)
        
        self.view.addSubview(imageView)
        
        imageView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        
        return imageView
    }
    
    // MARK: - Кнопка добавления города в избранное
    func makeSaveButtonView() -> UIImageView {
        let imageView = UIImageView()
        let largeFont = UIFont.systemFont(ofSize: 25)
        let configuration = UIImage.SymbolConfiguration(font: largeFont)
        imageView.image = UIImage(systemName: "plus.circle.fill", withConfiguration: configuration)
        imageView.tintColor = .white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(self.gestureSave)
        imageView.addGestureRecognizer(self.gestureDelete)
        
        self.view.addSubview(imageView)
        
        imageView.bottomAnchor.constraint(equalTo: self.findButtonView.topAnchor, constant: -8).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        
        return imageView
    }
    
    // MARK: - Кнопка обновления погоды по текущей геолокации
    func makeСLButtonView() -> UIImageView {
        let imageView = UIImageView()
        let largeFont = UIFont.systemFont(ofSize: 25)
        let configuration = UIImage.SymbolConfiguration(font: largeFont)
        imageView.image = UIImage(systemName: "location.circle.fill", withConfiguration: configuration)
        imageView.tintColor = .white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(self.gestureCL)
        
        self.view.addSubview(imageView)
        
        imageView.bottomAnchor.constraint(equalTo: self.findButtonView.topAnchor, constant: -8).isActive = true
        imageView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor, constant: 16).isActive = true
        
        return imageView
    }
    
    // MARK: - Установка иконки для кнопки сохранение/удаление
    func currentButtonState(_ systemName: String) -> UIImage {
        let largeFont = UIFont.systemFont(ofSize: 25)
        let configuration = UIImage.SymbolConfiguration(font: largeFont)
        let image = UIImage(systemName: systemName, withConfiguration: configuration)
        return image ?? UIImage()
    }
}

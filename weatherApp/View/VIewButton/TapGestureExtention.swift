//
//  TapGestureExtention.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit
import CoreData
import CoreLocation

extension MainViewController {
    
    // MARK: - Экшн кнопки поиска
    @objc func tapGestureFind(_ gusture: UITapGestureRecognizer) {
        guard self.gestureFind === gusture else { return }
        self.presentSearchAlertController(withTitle: "В каком городе найти погоду?", message: nil, style: .alert) { city in
            self.viewModel.updateCityWeather(city: city)
            self.mainView.loadingWeather()
        }
    }
    
    // MARK: - Экшен кнопки добавления
    @objc func tapGestureSave(_ gusture: UITapGestureRecognizer) {
        guard
            self.gestureSave === gusture,
            let currentCity = (self.mainView as! MainView).cityLable.text
        else { return }

        guard !self.cities.contains(where: { city in
            city.name == currentCity
        }) else {
            print("Такой город уже добавлен в избранное")
            return
        }

        guard let entity = NSEntityDescription.entity(forEntityName: "City", in: context) else { return }
        let cityObject = City(entity: entity, insertInto: context)
        cityObject.name = currentCity

        do {
            try self.context.save()
            cities.append(cityObject)
            self.collectionView.reloadData()
        } catch let error as NSError {
            printContent(error.localizedDescription)
        }
    }
    
    // MARK: - Экшн кнопки текущей локации
    @objc func tapGestureCL(_ gusture: UITapGestureRecognizer) {
        guard self.gestureCL === gusture else { return }
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.requestLocation()
            self.mainView.loadingWeather()
        }
    }
    
    // MARK: - Экшн переключения сохранение/удаления
    @objc func longPressGestureDelete(_ gusture: UILongPressGestureRecognizer) {
        guard self.gestureDelete === gusture else { return }
        if gusture.state == .began {
            self.isDeleteEnable.toggle()
            switch self.isDeleteEnable {
            case true:
                self.saveButtonView.image = self.currentButtonState("minus.circle.fill")
            case false:
                self.saveButtonView.image = self.currentButtonState("plus.circle.fill")
            }
        }
    }
}

//
//  GertureExtention.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 28.08.2022.
//

import UIKit

extension MainViewController {

    // MARK: - Жест поиска
    func makeGestureFind() -> UITapGestureRecognizer {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.tapGestureFind))
        return tapGesture
    }
    
    // MARK: - Жест добавления
    func makeGestureSave() -> UITapGestureRecognizer {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.tapGestureSave))
        return tapGesture
    }
    
    // MARK: - Жест текущей локации
    func makeGestureCL() -> UITapGestureRecognizer {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.tapGestureCL))
        return tapGesture
    }
    
    // MARK: - Жест удаления
    func makeGestureDelete() -> UILongPressGestureRecognizer {
        let longPressGesture = UILongPressGestureRecognizer()
        longPressGesture.minimumPressDuration = 1.5
        longPressGesture.addTarget(self, action: #selector(self.longPressGestureDelete))
        return longPressGesture
    }
}

//
//  File.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import UIKit
import CoreLocation

enum RequestType {
    case cityName(city: String)
    case coordinate(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
}

class NetwortService: NetworkServiceProtocol {
    
    var onComplition: ((WeatherModel) -> Void)?
    
    func fetchCurrentWeather(forRequestType requestType: RequestType) {
        var urlString = ""
        switch requestType {
        case .cityName(let city):
            urlString = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=\(apiKey)&units=metric&lang"
        case .coordinate(let latitude, let longitude):
            urlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&apikey=\(apiKey)&units=metric&lang"
        }
        performRequest(withURLString: urlString)
    }
    
    fileprivate func performRequest(withURLString urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, respounse, error in
            if let data = data {
                if let weatherModel = self.parseJSON(withData: data) {
                    self.onComplition?(weatherModel)
                }
            }
        }
        task.resume()
    }
    
    fileprivate func parseJSON(withData data: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        do {
            let weatherData = try decoder.decode(WeatherModelData.self, from: data)
            guard let weatherModel = WeatherModel(weatherModelData: weatherData) else {
                return nil
            }
            return weatherModel
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}

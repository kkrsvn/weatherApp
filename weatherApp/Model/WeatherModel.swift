//
//  WeatherModel;.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import Foundation

struct WeatherModel {
    let cityName: String
    let temperature: Double
    var temperatureString: String {
        return "\(Int(temperature)) \u{00B0}C"
    }
    let feelLikeTemprature: Double
    var feelLikeTempratureString: String {
        return "Ощущается как \(Int(feelLikeTemprature)) \u{00B0}C"
    }
    let conditionCode: Int
    var systemIconStirng: String {
        switch conditionCode {
        case 200...232: return "cloud.bolt.rain.fill"
        case 300...321: return "cloud.drizzle.fill"
        case 500...531: return "cloud.rain.fill"
        case 600...622: return "cloud.snow.fill"
        case 701...781: return "smoke.fill"
        case 800: return "sun.max.fill"
        case 801...804: return "cloud.fill"
        default: return "nosign"
        }
    }

    init?(weatherModelData: WeatherModelData) {
        self.cityName = weatherModelData.name
        self.temperature = weatherModelData.main.temp
        self.feelLikeTemprature = weatherModelData.main.feelsLike
        self.conditionCode = weatherModelData.weather.first!.id
    }
}

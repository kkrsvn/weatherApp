//
//  WeatherModelData.swift
//  weatherApp
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import Foundation

struct WeatherModelData: Codable {
    let name: String
    let main: Main
    let weather: [Weather]
}

struct Main: Codable {
    let temp, feelsLike: Double

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
    }
}

struct Weather: Codable {
    let id: Int
}

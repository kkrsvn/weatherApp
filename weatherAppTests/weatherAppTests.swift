//
//  weatherAppTests.swift
//  weatherAppTests
//
//  Created by Kirill Krasavin on 07.08.2022.
//

import XCTest
@testable import weatherApp

class MockView: UIView, MainViewProtocol {
    var cityName: String!
    
    var weatherData: WeatherModel?
    
    func setCurrentWeather(weatherModel: WeatherModel) {
        self.cityName = weatherModel.cityName
    }
    
    func loadingWeather() {
        print("Погода загружается")
    }
    
    func setWeather() {
        print("Погода загружена")
    }
}

class MockNetworkService: NetworkServiceProtocol {
    var onComplition: ((WeatherModel) -> Void)?
    
    func fetchCurrentWeather(forRequestType requestType: RequestType) {
        
        let testWeatherModelData = WeatherModelData(name: "Baz",
                                                    main: Main(temp: 10.0, feelsLike: 10.0),
                                                    weather: [Weather.init(id: 100)])
        let testWeatherModel = WeatherModel(weatherModelData: testWeatherModelData)
        
        if let model = testWeatherModel {
            self.onComplition?(model)
        }
    }
}

class weatherAppTests: XCTestCase {
    
    var mockView: MockView!
    var trueView: MainViewProtocol!
    var trueNetworkManager: NetworkServiceProtocol!
    var mockNetworkManager: NetworkServiceProtocol!
    var viewModel: MainViewModel!
    var trueViewModel: MainViewModel!
    var viewController: MainViewController!
    var trueViewController: MainViewController!

    override func setUpWithError() throws {
        mockView = MockView()
        trueView = MainView()
        trueNetworkManager = NetwortService()
        mockNetworkManager = MockNetworkService()
        viewModel = MainViewModel(networkService: mockNetworkManager)
        trueViewModel = MainViewModel(networkService: trueNetworkManager)
        viewController = MainViewController(mainView: mockView, viewModel: viewModel)
        trueViewController = MainViewController(mainView: trueView, viewModel: trueViewModel)
    }

    override func tearDownWithError() throws {
        mockView = nil
        trueView = nil
        mockNetworkManager = nil
        trueNetworkManager = nil
        viewModel = nil
        trueViewModel = nil
        viewController = nil
        trueViewController = nil
    }

    func testModuleIsNotNil() {
        XCTAssertNotNil(mockView, "View is not nil")
        XCTAssertNotNil(mockNetworkManager , "NetworkManager is not nil")
        XCTAssertNotNil(viewModel, "ViewModel is not nil")
        XCTAssertNotNil(viewController, "ViewController is not nil")
        
        XCTAssertNotNil(trueView, "View is not nil")
        XCTAssertNotNil(trueNetworkManager , "NetworkManager is not nil")
        XCTAssertNotNil(trueViewModel, "ViewModel is not nil")
        XCTAssertNotNil(trueViewController, "ViewController is not nil")
    }
    
    func testView() {
        mockNetworkManager.fetchCurrentWeather(forRequestType: .cityName(city: "Baz"))
        guard let data = mockView.weatherData else { return }
        mockView.setCurrentWeather(weatherModel: data)
        XCTAssertEqual(mockView.cityName, "Baz")
    }
    
    func testNetworkService() {
        trueNetworkManager.fetchCurrentWeather(forRequestType: .cityName(city: "Yaroslavl"))
        guard let cityName = trueView.weatherData?.cityName else { return }
        XCTAssertEqual(cityName, "Yaroslavl")
    }
}
